<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * AdminLTE asset bundle
 */
class AdminLteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';

    public $js = [
        'js/adminlte.js'
    ];

    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}