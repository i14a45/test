<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_`.
 */
class m180409_093058_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->decimal(10, 2)->defaultValue(0),
            'start_date' => $this->date()->notNull(),
            'finish_date' => $this->date()->notNull(),
        ]);

        $this->addForeignKey('fk-project-user_id', 'project', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-project-user_id', 'project');
        $this->dropTable('project');
    }
}
