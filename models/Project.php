<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $price
 * @property string $start_date
 * @property string $finish_date
 *
 * @property User $user
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'start_date', 'finish_date'], 'required'],
            [['start_date', 'finish_date'], 'date', 'format' => 'php:Y-m-d'],
            [['user_id'], 'integer'],
            [['price'], 'number'],
            [['start_date', 'finish_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название',
            'price' => 'Стоимость',
            'start_date' => 'Дата начала',
            'finish_date' => 'Дата завершения',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $dt = \DateTime::createFromFormat('d.m.Y', $this->start_date);
        if ($dt !== false) {
            $this->start_date = $dt->format('Y-m-d');
        }
        $dt = \DateTime::createFromFormat('d.m.Y', $this->finish_date);
        if ($dt !== false) {
            $this->finish_date = $dt->format('Y-m-d');
        }
        return true;
    }

    public function afterFind()
    {
        $this->start_date = Yii::$app->formatter->asDate($this->start_date, 'php:d.m.Y');
        $this->finish_date = Yii::$app->formatter->asDate($this->finish_date, 'php:d.m.Y');
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getStartDate()
    {
        return Yii::$app->formatter->asDate($this->start_date, 'php:d.m.Y');
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getFinishDate()
    {
        return Yii::$app->formatter->asDate($this->finish_date, 'php:d.m.Y');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
