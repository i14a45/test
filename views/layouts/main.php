<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="main-header">
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <?= \yii\widgets\Menu::widget([
                'items' => [
                    ['label' => 'Проекты', 'url' => ['/projects/index']],
                    ['label' => 'Пользователи', 'url' => ['/users/index']],
                    [
                        'label' => 'Выход',
                        'url' => ['/site/logout'],
                        'visible' => !Yii::$app->user->isGuest,
                        'template' => '<a href="{url}" data-method="post">{label}</a>',
                    ],
                ],
            ]); ?>
        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <h1><?= Html::encode($this->title); ?></h1>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    <?= Alert::widget(); ?>
                    <?= $content; ?>
                </div>
            </div>
        </section>
    </div>

    <footer class="main-footer">

    </footer>
</div>
); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
