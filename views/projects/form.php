<?php
/* @var $this yii\web\View */
/* @var $users array */
/* @var $model \app\models\Project */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'user_id')->dropDownList($users) ?>
    <?= $form->field($model, 'start_date')->widget(DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'readonly' => true,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>
    <?= $form->field($model, 'finish_date')->widget(DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'readonly' => true,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
