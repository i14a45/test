<?php

/* @var $this yii\web\View */
/* @var $searchModel \app\models\ProjectSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Проекты';
?>

<div class="user-index">

    <p>
        <?= Html::a('Новый проект', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user->name;
                }
            ],
            'start_date',
            'finish_date',


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
